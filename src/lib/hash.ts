import { SHA3 } from "sha3";

export enum Component {
  Digit = 0,
  Upper = 1,
  Lower = 2,
  Other = 3,
}

export enum Format {
  Char1 = 0,
  Char2 = 1,
  Char3 = 2,
  Char4 = 3,
  Char5 = 4,
  Char6 = 5,
  Char8 = 7,
  Ascii = -1,
  Rune8 = -2,
}

const chars = (first: number, last: number): string =>
  Array.from(Array(last - first + 1), (_, i) =>
    String.fromCharCode(first + i),
  ).join("");

const componentChars: Map<Component, string> = new Map([
  [Component.Digit, chars(48, 57)],
  [Component.Upper, chars(65, 90)],
  [Component.Lower, chars(97, 122)],
  [
    Component.Other,
    chars(33, 47) + chars(58, 64) + chars(91, 96) + chars(123, 126),
  ],
]);

const rune8 = (src: Uint8Array) =>
  String.fromCharCode(...Array.from(src).map((b) => b + (b > 94 ? 256 : 33)));

const patch = (
  seed: [number, number],
  chars: string,
  slots: number[],
  value: string,
): [string, number] => {
  const ch = chars[seed[0] % chars.length];
  const i = slots[seed[1] % slots.length] as number;
  const left = value.substring(0, i);
  const right = value.substring(i + 1, value.length);
  return [left + ch + right, i];
};

const patchedHashOf = (
  src: Uint8Array,
  value: string,
  // must be ordered (digit, upper, lower, other) :D
  goal: [Component, number][],
  index = 0,
  slots?: number[],
): string => {
  slots = slots || Array.from(Array(value.length), (_, i) => i);
  if (!slots.length) return value;
  if (!goal.length) return value;
  const count = new Map<Component, number>();
  for (const ch of value.split("")) {
    for (const [component, chars] of componentChars) {
      if (chars.includes(ch))
        count.set(component, (count.get(component) || 0) + 1);
    }
  }
  for (const [component, needed] of goal) {
    if ((count.get(component) || 0) >= needed) continue;
    const seed = [src[index], src[(index + 1) % src.length]] as [
      number,
      number,
    ];
    const chars = componentChars.get(component)!;
    const [patched, used] = patch(seed, chars, slots, value);
    slots.splice(slots.indexOf(used), 1); // why indexof :/
    // WTF
    const nextIndex = (index + 2) % src.length;
    return patchedHashOf(src, patched, goal, nextIndex, slots);
  }
  return value;
};

export const resolveHash = (
  src: string,
  fmt: Format,
  goal: [Component, number][],
  maxLength?: number,
): string => {
  const hash = new SHA3().update(src);
  const base = (() => {
    switch (fmt) {
      case Format.Ascii:
        return hash.digest("ascii");
      case Format.Char6:
        return hash.digest("base64");
      case Format.Char4:
        return hash.digest("hex");
      case Format.Char8:
        return new TextDecoder().decode(hash.digest("binary"));
      case Format.Rune8:
        return rune8(hash.digest("binary"));
      default:
        throw "unimplemented";
    }
  })();
  const result = patchedHashOf(hash.digest("binary"), base, goal);
  if (maxLength) return result.substring(0, maxLength);
  return result;
};
