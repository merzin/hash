if ("virtualKeyboard" in navigator) {
  const virtualKeyboard = navigator.virtualKeyboard as any;
  virtualKeyboard.overlaysContent = true;
  virtualKeyboard.addEventListener("geometrychange", (event: any) => {
    const keyboardHeight = event.target.boundingRect.height;
    document.documentElement.style.setProperty(
      "--keyboard-height",
      `${keyboardHeight}px`,
    );
  });
}
