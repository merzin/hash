/// <reference lib="webworker" />
/// <reference types="@sveltejs/kit" />
import { build, files, version } from "$service-worker";

declare const self: ServiceWorkerGlobalScope;

const VERSION = `version-${version}`;
const ASSETS = ["/", ...build, ...files.filter((f) => !f.startsWith("/_"))];

self.addEventListener("install", (event) => event.waitUntil(install()));
self.addEventListener("activate", (event) => event.waitUntil(activate()));
self.addEventListener("fetch", (event) =>
  event.respondWith(match(event.request)),
);

async function install() {
  self.skipWaiting();
  const cache = await caches.open(VERSION);
  await cache.addAll(ASSETS);
  await deleteOldVersions();
}

async function activate() {
  self.clients.claim();
  await deleteOldVersions();
}

async function match(request: Request) {
  const response = await caches.match(request);
  if (response) return response;
  return fetch(request);
}

async function deleteOldVersions() {
  const versions = await getInstalledVersions();
  const past_versions = versions.filter((key) => key !== VERSION);
  await Promise.all(past_versions.map((key) => caches.delete(key)));
}

async function getInstalledVersions(): Promise<string[]> {
  const keys = await caches.keys();
  return keys.filter((key) => key.startsWith("version-"));
}
